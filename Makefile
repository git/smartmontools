PACKAGE=smartmontools
VER=7.2
DEBREL=pve1

SRCDIR=smartmontools

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)
GITVERSION:=$(shell cat .git/refs/heads/master)

DEB=${PACKAGE}_${VER}-${DEBREL}_${ARCH}.deb

all: ${DEB}
	@echo ${DEB}

.PHONY: deb
deb ${DEB}: | submodule
	rm -rf ${SRCDIR}.tmp
	cp -rpa ${SRCDIR} ${SRCDIR}.tmp
	rm -rf ${SRCDIR}.tmp/.git
	cd ${SRCDIR}.tmp; patch -p1 < ../patches/update_changelog.patch
	echo "git clone git://git.proxmox.com/git/smartmontools.git\\ngit checkout ${GITVERSION}" >  ${SRCDIR}.tmp/debian/SOURCE
	echo "debian/SOURCE" >> ${SRCDIR}.tmp/debian/docs
	cd ${SRCDIR}.tmp; dpkg-buildpackage -rfakeroot -b -uc -us
	lintian ${DEB}

# make sure submodule was initialized
.PHONY: submodule
submodule:
	test -f "${SRCDIR}/debian/changelog" || git submodule update --init

# upgrade to current master
.PHONY: download
download ${SRCDIR}: submodule
	git submodule foreach 'git pull --ff-only origin master'

.PHONY: upload
upload: ${DEB}
	tar cf - ${DEB} | ssh repoman@repo.proxmox.com upload

distclean: clean

.PHONY: clean
clean:
	rm -rf ${SRCDIR}.tmp *_${ARCH}.deb *.changes *.dsc *.buildinfo
	find . -name '*~' -exec rm {} ';'

.PHONY: dinstall
dinstall: ${DEB}
	dpkg -i ${DEB}
